# 版權聲明
我們在打造此平台/軟體時用上了許多超讚的開源套件和免費素材，沒有這些無法造就今天的「看路」，此頁中將列出我們所使用的所有套件，感謝這些作者們！

## Icons

**[Icons](https://www.flaticon.com/free-icon/bollard_648717) made by [Becris](https://www.flaticon.com/authors/becris) from [flaticon.com](https://www.flaticon.com)**<br>
<img src="https://image.flaticon.com/icons/svg/648/648717.svg" width="64">

**Icons made by [dDara](https://www.flaticon.com/authors/dDara) from [flaticon.com](https://www.flaticon.com/)**<br>
<img src="https://image.flaticon.com/icons/svg/1087/1087436.svg" width="64">

**Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [flaticon.com](https://www.flaticon.com/)**<br>
<img src="https://image.flaticon.com/icons/svg/588/588019.svg" width="64">
<img src="https://image.flaticon.com/icons/svg/869/869869.svg" width="64">
<img src="https://image.flaticon.com/icons/svg/42/42877.svg" width="64">
<img src="https://image.flaticon.com/icons/svg/42/42874.svg" width="64">

**Icons made by [Pixel Perfect](https://www.flaticon.com/authors/pixel-perfect) from [flaticon.com](https://www.flaticon.com/)**<br>
<img src="https://image.flaticon.com/icons/svg/726/726498.svg" width="64">

**Icons made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [flaticon.com](https://www.flaticon.com/)**<br>
<img src="https://image.flaticon.com/icons/svg/660/660333.svg" width="64">

---

## Android
以下列出 Android 客戶端所使用之套件：
- Apache Common IO
- Facebook Android SDK (Everything)
- Facebook Login SDK
- Firebase Analytics Ktx
- Firebase Auth Ktx
- Firebase Config Ktx
- Firebase Core Ktx
- Firebase Crashlytics Ktx
- Firebase Firestore
- Firebase UI Auth
- Firebase UI Firestore
- Glide
- GSon
- Kotlin Standard Library
- KotlinX Coroutines Core
- KotlinX Coroutines Android
- Material Components for Android
- Retrofit2
- TouchImageView

#### Apache Common IO
https://github.com/apache/commons-io

#### Facebook Android SDK (Everything)
https://github.com/facebook/facebook-android-sdk

#### Facebook Login SDK
https://github.com/facebook/facebook-android-sdk/tree/master/facebook-login

#### Firebase Config Ktx
https://github.com/firebase/firebase-android-sdk/tree/master/firebase-config

#### Firebase Crashlytics Ktx
https://github.com/firebase/firebase-android-sdk/tree/master/firebase-crashlytics

#### Firebase Firestore
https://github.com/firebase/firebase-android-sdk/tree/master/firebase-firestore

#### Firebase UI Auth
https://github.com/firebase/FirebaseUI-Android/tree/master/auth

#### Firebase UI Firestore
https://github.com/firebase/FirebaseUI-Android/tree/master/firestore

#### Glide
https://github.com/bumptech/glide

#### GSon
https://github.com/google/gson<br>

#### Kotlin Standard Library
https://github.com/JetBrains/kotlin/tree/master/libraries/stdlib<br>

#### KotlinX Coroutines Core
https://github.com/Kotlin/kotlinx.coroutines

#### KotlinX Coroutines Android
https://github.com/Kotlin/kotlinx.coroutines/tree/master/ui/kotlinx-coroutines-android

#### Material Components for Android
https://github.com/material-components/material-components-android

#### Retrofit2
https://github.com/square/retrofit

#### TouchImageView
https://github.com/MikeOrtiz/TouchImageView

---

## iOS
以下列出 iOS 客戶端所使用之套件：

---

## Web
以下列出 Web 客戶端所使用之套件：
- Axios
- Core JS
- Firebase JS SDK
- Vue
- Vue-Axios
- Vue-Cookies
- Vue-Router
- Vuefire
- Vuetify
- Vuex

#### Axios
https://github.com/axios/axios

#### Core JS
https://github.com/zloirock/core-js

#### Firebase JS SDK
https://github.com/firebase/firebase-js-sdk

#### Vue
https://github.com/vuejs/vue

#### Vue-Axios
https://github.com/imcvampire/vue-axios

#### Vue-Cookies
https://github.com/cmp-cc/vue-cookies

#### Vue-Router
https://github.com/vuejs/vue-router

#### Vuefire
https://github.com/vuejs/vuefire

#### Vuetify
https://github.com/vuetifyjs/vuetify

#### Vuex
https://github.com/vuejs/vuex