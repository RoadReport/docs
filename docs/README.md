# 看路！ - 為山路而生的路況回報平台

?> 南部熱門山路的路況、天氣和即時影像

!> 本平台仍在開發階段，目前開放使用路段為**台 24 (aka. 霧台)** 與 **182 縣道**，歡迎加入公測！

## 安裝
[點我前往安裝頁面](zh-TW/GetStart/Install.md)，或者往下轉看看「看路！」的簡介。

## 預覽
<img width="240" src="https://lh3.googleusercontent.com/CGF1BUYzTtb-UMa7IhyzJAWhHu9-lB1uPxy2BY3WBKFVeQXtZYnSpqqRXJuH0XC8rIwh=w2560-h1428">
<img width="240" src="https://lh3.googleusercontent.com/ddYH7tLQ4MePtfyMPPR61Pzcy4f2B2DmHTThaeOwyeNMMFayT1RufQ6sWg2G4155dBTQ=w2560-h1428">
<img width="240" src="https://lh3.googleusercontent.com/eNfFu2T_6mBwMb_RBgSe9uuC04nW1dILA76sLd0NjVF1G4kJeDzxoP6OBWfln8NC_Us=w2560-h1428">

## 簡介
簡單來說，就是幫你把 **路況**、**天氣**、**即時影像** 這些資訊整合到一個 APP 當中。

✔️ 主要功能如下：
- 即時回報與接收路況資訊，如事故、測速、臨檢、天氣等
- 支援地圖標點，新增事件時可在地圖上選位，其他人點擊地點文字就能查看位置囉！
- 整合道路附近即時天氣資訊，包含氣溫和降雨量
- 整合路上的監視器即時影像
- 支援淺色 / 深色主題

## 目前有開放哪些路段？

目前仍在開發階段，開放路段如下：
- **台 24 縣**：位於霧台鄉

- **182 縣道**：位於台南龍崎與高雄旗山交界

## 那麼，哪裡可以安裝呢？
[這裡！點我前往安裝頁面！](zh-TW/GetStart/Install.md)

## ✏️ 版權資訊
請見：https://bit.ly/RoadRLicense

## 🎤 我對平台有意見，哪裡可以出來瞧？

你可以在 Discord 上與大家聯繫：https://discord.gg/czhCDBD

## 💻 開源計畫，歡迎一同維護
https://bit.ly/RoadRAbout