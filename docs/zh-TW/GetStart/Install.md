# 安裝

## Android
#### 安裝說明
在 Play Store 搜尋 「看路」或點擊下方直接前往！<br>
<a target="_blank" href="https://bit.ly/3oGNPb1">
    <img src="https://play.google.com/intl/en_us/badges/static/images/badges/zh-tw_badge_web_generic.png" width="256">
</a>

目前提供版本：
- [ ] 正式版
- [x] 公開測試版
- [x] 內測版


## iOS
!> iOS 版本將視「看路」平台整體使用狀況進行開發，換言之，用的人多就會開發，沒人用就不會開發。<br>
(主要還是上架費太貴，而且每年都要繳 QAQ)

現階段請先使用 Web 版本代替，不過別擔心，我們已經把 Web 版本弄成類似 APP 了！


## Web
#### 安裝說明
1. 首先，[前往看路網頁版](https://bit.ly/2KeuXS0)
2. 按下瀏覽器中的 **分享**
3. 選擇 **加入主畫面**
4. 完成，現在「看路」就像一個 APP 存在您桌面上了！
