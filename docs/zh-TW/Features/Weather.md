# 天氣

!> 不保證資料正確性及實際情況，即時氣象站資料由政府單位提供，一切資料僅供參考！

本軟體/網站附帶之天氣功能資料來源為：
- [氣象資料開放平台](https://opendata.cwb.gov.tw/index)
- [自動氣象站-氣象觀測資料](https://opendata.cwb.gov.tw/dataset/observation/O-A0001-001)
- [自動雨量站-雨量觀測資料](https://opendata.cwb.gov.tw/dataset/observation/O-A0002-001)