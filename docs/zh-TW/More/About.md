# 關於
所有客戶端皆為開源，到 [GitHub / RoadReport](https://github.com/roadreport) 了解更多資訊

## Android Client
最新版本：`Beta 1.4.5`

#### 最新版更新紀錄
- 改善即時影像介面，把影像畫面固定，剩下列表可以捲動
- 其他頁面 UI/UX 微調
- 新增「落石」和「坍方」兩種事件種類

[歷史更新紀錄](zh-TW/More/ChangeLogAndroid.md)

#### Open Source
?> [RoadReprot / AndroidClient](https://github.com/MrNegativeTW/RoadReport_AndroidClient)

#### Developers
- [Trevor Wu](https://github.com/MrNegativeTW)
  
---

## iOS Client
最新版本：`null`

iOS App 尚無開發計畫，將視平台整體使用情況決定是否開發，或等待熱心的開發者跳進來這個坑。

#### Open Source

?> [RoadReport / iOSClient](https://github.com/roadreport/roadreport_webclient2)

#### Developers
- 無，快成為第一位開發者吧！

---

## Web Client
最新版本：`0.3.0`

#### 最新版更新紀錄
- 新增「落石」和「坍方」兩種事件種類

[歷史更新紀錄](zh-TW/More/ChangeLogWeb.md)

#### Open Source
?> [RoadReport / WebClient2](https://github.com/roadreport/roadreport_webclient2)

#### Developers
- [Trevor Wu](https://github.com/MrNegativeTW)
- [Kevin Lee](https://github.com/MrKevinLee)