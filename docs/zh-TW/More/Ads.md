# 廣告

### 這不是免費軟體嗎？為什麼有廣告？<br>
A: 因為背後支撐服務的資料庫需要費用。

1. 資料庫目前為免費方案，若達用量上限服務將會中斷，為避免此情形發生，我們在 APP 中放了點廣告，以供日後升級為付費版時支撐流量。
2. iOS 上架費不便宜，廣告也將於未來用於支付上架費用。

資料庫詳細價格請參閱：
[Firebase Pricing](https://firebase.google.com/pricing)

### 關於廣告服務供應商
我們使用 Google AdMob / Google AdSense 作為廣告供應商，**理論上來說**若您有於裝置上有登入您的 Google 帳號，此舉將使您 Google 帳戶設定中的[廣告個人化](https://adssettings.google.com.hk/authenticated?pli=1)功能繼續沿用於本軟體/網站。

### 你賺很多對不對
下圖為收益報表。

<img src="https://i.imgur.com/JFXcoYj.png" width="480">